# Resources

Here are some great resources for learning or improving Ruby skills:

* IRC: ##new2ruby on freenode
* [TryRuby.org](http://tryruby.org)
* [why's (poignant) guide to Ruby](http://www.rubyinside.com/media/poignant-guide.pdf)
* [RubyWarrion](https://www.bloc.io/ruby-warrior/#/)

